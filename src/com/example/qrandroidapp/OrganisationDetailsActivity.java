package com.example.qrandroidapp;

import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;

import model.Organisation;
import utils.CreateVCard;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class OrganisationDetailsActivity extends Activity {
	
	private ImageView qrImage = null;
	private TextView organisationName = null;
	private TextView organisationEmail = null;
	private TextView organisationAddress = null;
	private TextView organisationWeb = null;
	private TextView organisationPhone = null;
	private TextView organisationMobile = null;
	private TextView organisationFax = null;
	private TextView organisationCountry = null;
	private TextView organisationBranch = null;

	private Organisation organisation = null;
	
	long id = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.organisation_details_activity);
		
		qrImage = (ImageView) findViewById(R.id.organisation_qr_img);
		organisationName = (TextView) findViewById(R.id.organisation_name);
		organisationEmail = (TextView) findViewById(R.id.organisation_email);
		organisationAddress = (TextView) findViewById(R.id.organisation_address);
		organisationWeb = (TextView) findViewById(R.id.web_page);
		organisationPhone = (TextView) findViewById(R.id.phone);
		organisationMobile = (TextView) findViewById(R.id.mobile_phone);
		organisationFax = (TextView) findViewById(R.id.fax);
		organisationCountry = (TextView) findViewById(R.id.country);
		organisationBranch = (TextView) findViewById(R.id.branch);
		
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			organisation = extras.getParcelable("organisation");
		}
		
		String qrData = CreateVCard.createOrganisationVCard(organisation);
		
		if (organisation.getBranch() == null) {
			organisation.setBranch(getOrganisationBranch(organisation.getBranch_id()));
		}
		
		if (organisation.getCountry() == null) {
			organisation.setCountry(getOrganisationCountry(organisation.getCountry_id()));
		}
		
		organisationName.setText(organisation.getName());
		organisationEmail.setText(organisation.getEmail());
		organisationAddress.setText(organisation.getAddress());
		organisationWeb.setText(organisation.getWebPage());
		organisationPhone.setText(organisation.getPhone());
		organisationMobile.setText(organisation.getMobile());
		organisationFax.setText(organisation.getFax());
		organisationCountry.setText(organisation.getCountry());
		organisationBranch.setText(organisation.getBranch());
		
		Bitmap bitmap = null;
		try {
			bitmap = encodeAsBitmap(qrData, BarcodeFormat.QR_CODE, 300, 300);
			qrImage.setImageBitmap(bitmap);
		} catch (WriterException e) {
	        e.printStackTrace();
	    }
	}
	
	public void sendEmail(View v) {
		if (organisationEmail.getText().equals("")) {
			Toast.makeText(this, "Email address is not available", Toast.LENGTH_SHORT).show();
			return;
		}
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{organisationEmail.getText().toString() });
		startActivity(emailIntent);
	}
	
	public void openWeb(View v) {
		if (organisationWeb.getText().equals("")) {
			Toast.makeText(this, "Web page is not available", Toast.LENGTH_SHORT).show();
			return;
		}
		String url = organisationWeb.getText().toString();
		Intent browserIntent = new Intent(Intent.ACTION_VIEW);
		browserIntent.setData(Uri.parse(url));
		startActivity(browserIntent);
	}
	
	public void callPhone(View v) {
		if (organisationPhone.getText().equals("")) {
			Toast.makeText(this, "Phone is not available", Toast.LENGTH_SHORT).show();
			return;
		}
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + organisationPhone.getText().toString()));
		startActivity(callIntent);
	}
	
	public void callMobile(View v) {
		if (organisationMobile.getText().equals("")) {
			Toast.makeText(this, "Mobile phone is not available", Toast.LENGTH_SHORT).show();
			return;
		}
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + organisationMobile.getText().toString()));
		startActivity(callIntent);
	}
	
	public void findLocation(View v) {
		if (organisation.getGpsLongtitude() == 0.0 && organisation.getGpsLatitude() == 0.0) {
			Toast.makeText(this, "GPS coordinates are not available", Toast.LENGTH_SHORT).show();
		}
		String uri = String.format(Locale.ENGLISH, "geo:0,0?q="+organisation.getGpsLatitude()+","+organisation.getGpsLongtitude()+" (" + organisation.getAddress() + ")");
		Intent googleMapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		startActivity(googleMapIntent);
	}
	
	private static final int WHITE = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;

	Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width,
			int img_height) throws WriterException {
		String contentsToEncode = contents;
		if (contentsToEncode == null) {
			return null;
		}
		Map<EncodeHintType, Object> hints = null;
		String encoding = guessAppropriateEncoding(contentsToEncode);
		if (encoding != null) {
			hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
			hints.put(EncodeHintType.CHARACTER_SET, encoding);
		}
		MultiFormatWriter writer = new MultiFormatWriter();
		BitMatrix result;
		try {
			result = writer.encode(contentsToEncode, format, img_width,
					img_height, hints);
		} catch (IllegalArgumentException iae) {
			// Unsupported format
			return null;
		}
		int width = result.getWidth();
		int height = result.getHeight();
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			int offset = y * width;
			for (int x = 0; x < width; x++) {
				pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
			}
		}

		Bitmap bitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}

	private static String guessAppropriateEncoding(CharSequence contents) {
		// Very crude at the moment
		for (int i = 0; i < contents.length(); i++) {
			if (contents.charAt(i) > 0xFF) {
				return "UTF-8";
			}
		}
		return null;
	}
	
	private String getOrganisationBranch(int id) {
		String branch = null;
		Resources res = getResources();
		String[] branches = res.getStringArray(R.array.branches);
		
		for (int i = 0; i < branches.length; i++) {
			if (i + 1 == id) {
				branch = branches[i];
			}
		}
		return branch;
	}
	
	private String getOrganisationCountry(int id) {
		String country = null;
		Resources res = getResources();
		String countries[] = res.getStringArray(R.array.countries);
		
		for(int i = 0; i < countries.length; i++) {
			if (i + 1 == id) {
				country = countries[i];
			}
		}
		return country;
	}
	
}
