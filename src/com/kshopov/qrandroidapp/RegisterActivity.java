package com.kshopov.qrandroidapp;

import http.HttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.qrandroidapp.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	private EditText email = null;
	private EditText password = null;
	private EditText passwordAgain = null;
	private EditText username = null;
	private ProgressDialog dl = null;
	
	
	private static final String URL = "http://10.0.2.2:80/QRAppWeb/root/mobile_registration.php";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_activity);

		email = (EditText) findViewById(R.id.email_input);
		password = (EditText) findViewById(R.id.password_input);
		passwordAgain = (EditText) findViewById(R.id.password_again);
		username = (EditText) findViewById(R.id.username);

	}

	//TODO have to send data to server
	public void register(View v) {
		if (validate()) {
			dl = ProgressDialog.show(this, "Sendig data", "");
			SendData sendData = new SendData();
			sendData.execute(URL);
		}
	}

	private boolean validate() {
		if (!validation.Validator.isValidEmail(email)) {
			email.setError(getText(R.string.not_valid_email));
			email.requestFocus();
			return false;
		} else if (validation.Validator.isEmpty(username)) {
			username.setError(getText(R.string.empty_field));
			username.requestFocus();
		} else if (!validation.Validator.isSamevalue(password, passwordAgain)) {
			password.setError(getText(R.string.password_not_match));
			password.requestFocus();
			return false;
		}
		return true;
	}
	
	private class SendData extends AsyncTask<String, Void, JSONObject> {

		@Override
		protected JSONObject doInBackground(String... params) {
			JSONObject jsonObject = new JSONObject();
			JSONObject results = null;
			String url = params[0];
			try {
				jsonObject.put("username", username.getText().toString());
				jsonObject.put("email", email.getText().toString());
				jsonObject.put("password", password.getText().toString());
				jsonObject.put("passwordAgain", passwordAgain.getText().toString());				
				jsonObject.put("isOrganisation", 1);				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			results = HttpClient.SendHttpPost(url, jsonObject);
			
			return results;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			super.onPostExecute(result);
			if (result == null) {
				Toast.makeText(RegisterActivity.this, "Server error", Toast.LENGTH_SHORT).show();
			} else {
				try {
					switch (result.getInt("serverCode")) {
						case 0:
							username.setError(getText(R.string.empty_field));
							username.requestFocus();
						break;
						case 1:
							username.setError(getText(R.string.username_exists));
							username.requestFocus();
						break;
						case 2:
							password.setError(getText(R.string.password_not_match));
							password.requestFocus();
							break;
						case 3:
							email.setError(getText(R.string.email_exists));
							email.requestFocus();
							break;
						case 4:
							email.setError(getText(R.string.not_valid_email));
							email.requestFocus();
							break;
						case 10:
							Toast.makeText(RegisterActivity.this, R.string.successful_registration, Toast.LENGTH_LONG).show();
						default:
							//default 
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			dl.hide();
		}
		
	}
	
}

